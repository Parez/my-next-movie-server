# Movies REST API

## Install the dependencies

In the project root folder run `npm install`

## Run the project locally

Run `npm start`. The API will start running on `localhost:3000`.
The code will recompile on changes and server will be updated on-the-fly.

## Build the project

Run `npm run build`. The project will be built in the `dist` directory.
Then run `node dist/index.js` to start the server. The API will start running on `localhost:3000`.
You can provide environment variables when running node command.

## Database

The API will access local MongoDB by default at `localhost:27017`.

You can check the running API at `https://api.my-next-movie.com`.

