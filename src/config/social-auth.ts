/**
 * Created by baunov on 07/11/2017.
 */
const socialAuth = {

    'facebookAuth' : {
        'clientID'      : '859586494210058', // your App ID
        'clientSecret'  : '5af76a5214cd0a23ffdb57ebac0be79e', // your App Secret
        'callbackURL'   : 'http://localhost:8080/auth/facebook/callback',
        'profileFields' : ['id', 'displayName', 'photos', 'email', 'hometown', 'link']
    },

    'twitterAuth' : {
        'consumerKey'       : 'your-consumer-key-here',
        'consumerSecret'    : 'your-client-secret-here',
        'callbackURL'       : 'http://localhost:8080/auth/twitter/callback'
    },

    'googleAuth' : {
        'clientID'      : 'your-secret-clientID-here',
        'clientSecret'  : 'your-client-secret-here',
        'callbackURL'   : 'http://localhost:8080/auth/google/callback'
    }

};

export default socialAuth;