/**
 * Created by baunov on 08/11/2017.
 */
import * as mongoose from 'mongoose';
import {Observable} from 'rxjs/Observable';

export interface IUserCollection {
    movieId: number;
    userId: string;
}

interface IUserCollectionModel extends IUserCollection, mongoose.Document {}

export const UserCollectionSchema: mongoose.Schema = new mongoose.Schema({
    movieId: {
        type: Number
    },
    userId: {
        type: String
    }
});

const UserCollectionModel = mongoose.model<IUserCollectionModel>('collections', UserCollectionSchema);

export class UserCollection extends UserCollectionModel {

    constructor(item: IUserCollectionModel) {
        super();
        if (!item) {
            return;
        }
        this.movieId = item.movieId;
        this.userId = item.userId;
    }

    public static getCollection(userId: string, skip: number = 0, limit: number = 100): Observable<number[]> {
        return Observable.defer(() => {
            return UserCollection.find({userId}).skip(skip).limit(limit);
        }).map( (items) => {
            return items.map((item) => item.movieId);
        });
    }

    public static addToCollection(userId: string, movieId: number): Observable<any> {
        return Observable.defer( () => {
           return UserCollection.create({userId, movieId});
        });
    }

    public static removeFromCollection(userId: string, movieId: number): Observable<any> {
        return Observable.defer(() => {
            return UserCollection.findOneAndRemove({userId, movieId});
        });
    }

    public static isInCollection(userId: string, movieId: number): Observable<boolean> {
        return Observable.defer(() => {
            return UserCollection.findOne({userId, movieId});
        }).map((movie) => {
            return !!movie;
        });
    }
}

