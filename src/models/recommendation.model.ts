/**
 * Created by baunov on 08/11/2017.
 */
import * as mongoose from 'mongoose';
import {MovieInfo} from './movie-info.model';

export interface IRecommendation {
    TitleScore: number;
    DecadeScore: number;
    imdbID: number;
    userId: string;
    PlotScore: number;
    ProductionsScore: number;
    WritersScore: number;
    CountriesScore: number;
    GenresScore: number;
    ActorsScore: number;
    DirectorsScore: number;
    score: number;
}

export interface IRecommendationModel extends IRecommendation, mongoose.Document {}

export const RecommendationSchema: mongoose.Schema = new mongoose.Schema({
    TitleScore: {
        type: Number
    },
    DecadeScore: {
        type: Number
    },
    imdbID: {
        type: Number
    },
    userId: {
        type: String
    },
    PlotScore: {
        type: Number
    },
    GenresScore: {
        type: Number
    },
    CountriesScore: {
        type: Number
    },
    ActorsScore: {
        type: Number
    },
    DirectorsScore: {
        type: Number
    },
    ProductionsScore: {
        type: Number
    },
    WritersScore: {
        type: Number
    },
    score: {
        type: Number
    }
});

export const RecommendationModel = mongoose.model<IRecommendationModel>('recommendations', RecommendationSchema, 'recommendations');

export class Recommendation extends RecommendationModel {

    constructor(scores: IRecommendation) {
        super();
        if (!scores) {
            return;
        }
        this.TitleScore = scores.TitleScore;
        this.DecadeScore = scores.DecadeScore;
        this.imdbID = scores.imdbID;
        this.userId = scores.userId;
        this.GenresScore = scores.GenresScore;
        this.PlotScore = scores.PlotScore;
        this.CountriesScore = scores.CountriesScore;
        this.ActorsScore = scores.ActorsScore;
        this.DirectorsScore = scores.DirectorsScore;
        this.ProductionsScore = scores.ProductionsScore;
        this.WritersScore = scores.WritersScore;
        this.score = scores.score;
    }

    public static async getRecommendations(userId: string) {
        try {
            const scores: Recommendation[] = await Recommendation.find({userId});

            const moviePromises: Promise<MovieInfo>[] = scores.sort((s1, s2) => {
                if (s1.score > s2.score) {
                    return -1;
                } else {
                    return 1;
                }
            }).map(async (score: IRecommendation) => {
                return MovieInfo.findOne({imdbID: score.imdbID});
            });

            const recMovies: MovieInfo[]  = await Promise.all(moviePromises);

            return recMovies.map(movie => {
              const movieScores = scores.filter(score => score.imdbID === movie.imdbID)[0].toObject();
              return Object.assign({}, movieScores, {movie: movie.toObject()});
            })
        } catch (err) {

        }

    }
}

