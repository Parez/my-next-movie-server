/**
 * Created by baunov on 08/11/2017.
 */
import * as mongoose from 'mongoose';
import * as bcrypt from 'bcrypt';

export interface IFacebookProfile {
    gender: string;
    id: string;
    displayName: string;
    token: string;
}

export interface IUser {
    username: string;
    email: string;
    password: string;
    facebook: IFacebookProfile;
}

interface IUserModel extends IUser, mongoose.Document {}

export const UserSchema: mongoose.Schema = new mongoose.Schema({
    username: {
        type: String,
        required: true
    },
    email: {
        type: String,
        unique: true,
        required: true,
        trim: true
    },
    password: {
        type: String
    },
    facebook: {
        id: String,
        token: String,
        displayName: String,
        gender: String
    }
});

UserSchema.pre('validate', function (next) {
    const user = this;
    if (user.password) {
        bcrypt.hash(user.password, 10, function (err, hash){
            console.log('Hashed password', hash);
            if (err) {
                return next(err);
            }
            user.password = hash;
            next();
        });
    }
});

const UserModel = mongoose.model<IUserModel>('users', UserSchema);

export class User extends UserModel {
    username: string;
    email: string;
    password: string;
    facebook: IFacebookProfile;

    constructor(user: IUserModel) {
        super();
        if (!user) return;
        this.username = user.username;
        this.email = user.email;
        this.password = user.password;
        this.facebook = user.facebook;
    }
}

