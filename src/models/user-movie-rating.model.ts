/**
 * Created by baunov on 08/11/2017.
 */
import * as mongoose from 'mongoose';
import {Observable} from 'rxjs/Observable';
import {IMovieInfo, MovieInfo} from './movie-info.model';
import * as request from 'request';
import {Recommendation} from './recommendation.model';

export interface IUserMovieRating {
    movieId: number;
    userId: string;
    rating: number;
}

interface IUserMovieRatingModel extends IUserMovieRating, mongoose.Document {}

export const UserMovieRatingSchema: mongoose.Schema = new mongoose.Schema({
    movieId: {
        type: Number
    },
    userId: {
        type: String
    },
    rating: {
        type: Number
    }
});

const UserMovieRatingModel = mongoose.model<IUserMovieRatingModel>('ratings', UserMovieRatingSchema);

export class UserMovieRating extends UserMovieRatingModel {

    constructor(item: IUserMovieRatingModel) {
        super();
        if (!item) {
            return;
        }
        this.movieId = item.movieId;
        this.userId = item.userId;
        this.rating = item.rating;
    }

    public static getUserRatings(userId: string): Observable<any[]> {
        return Observable.defer(() => {
            return UserMovieRating.find({userId});
        });
    }

    public static async rateMovie(userId: string, movieId: number, rating: number): Promise<any> {
        const numBefore = await UserMovieRating.countReactions(userId);
        await UserMovieRating.findOneAndUpdate({userId, movieId}, {
          userId,
          movieId,
          rating
        }, {upsert: true});
        const num = await UserMovieRating.countReactions(userId);

        if (num >= 10 && num % 5 === 0 && num !== numBefore) {
          console.log("============= Calculate Recommendations ===========");
          await Recommendation.deleteMany({userId});
          try {
            const sparkUrl = `https://spark.my-next-movie.com/recs?userId=${userId}&apiKey=${process.env.SPARK_API_KEY}`;
            request.get(sparkUrl, {rejectUnauthorized: false}, (res) => {
              console.log(res);
            });
          } catch (err) {
            console.log(err);
          }
        }

        return num;
    }

    public static async getLikes(userId: string): Promise<IMovieInfo[]> {
        const likes = await UserMovieRating.find({userId, rating: 1});
        return Promise.all(likes.map(like => MovieInfo.findOne({imdbID: like.movieId})));
    }

    public static async getDislikes(userId: string): Promise<IMovieInfo[]> {
      const dislikes =  await UserMovieRating.find({userId, rating: -1});
      return Promise.all(dislikes.map(dislike => MovieInfo.findOne({imdbID: dislike.movieId})));
    }

    public static async countReactions(userId: string): Promise<number> {
      return await UserMovieRating.count({userId});
    }
}

