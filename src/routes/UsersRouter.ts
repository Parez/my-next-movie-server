import {Router, Request, Response, NextFunction} from 'express';
import { User } from '../models';
import * as passport from 'passport';
import {generateToken, sendAuth} from '../auth/jwt';
import {authenticate} from '../auth/token-auth';
import * as bcrypt from 'bcrypt';

export class UsersRouter {
    router: Router;

    /**
     * Initialize the HeroRouter
     */
    constructor() {
        this.router = Router();
        this.init();
    }

    public getCurrentUser(req, res, next) {
        console.log('ID', req.auth.id);
        User.findOne({_id : req.auth.id}).then( (user) => {
            console.log(user);
            return res.status(200).send({
                username: user.username,
                email: user.email,
                id: user._id
            });
        }).catch( (err) => {
            console.log(err);
            return res.status(401).send(err);
        });
    }

    public authFacebook(req: Request, res: Response, next: NextFunction) {
        if (!req.user) {
            return res.status(401).send('User Not Authenticated');
        }
        req['auth'] = {
            id: req.user.id,
            username: req.user.username,
            email: req.user.email
        };
        next();
    }

    public async register(req: Request, res: Response, next: NextFunction) {
        console.log('Register');
        if (req.body.email && req.body.username && req.body.password) {
            const userData = {
                email: req.body.email,
                username: req.body.username,
                password: req.body.password
            };
            // use schema.create to insert data into the db

            const existingUser = await User.findOne({email: req.body.email});

            if (existingUser) {
                return res.status(409).send('User with such email is already registered');
            }

            User.create(userData).then(user => {
                console.log(user);
                req['auth'] = {
                    id: user._id,
                    username: user.username,
                    email: user.email
                };
                next();
            }).catch(err => {
                return res.status(500).send(err);
            });
        }
    }

    public checkAuth(req: Request, res: Response, next: NextFunction) {
        const status = res.statusCode;
        if (req['auth'].id) {
          return res.json({
            status,
            data: 'Ok'
          })
        } else {
          return res.json({
            status,
            error: 'Not authenticated'
          })
        }
    }

    public async login(req: Request, res: Response, next: NextFunction) {
        console.log('Login');
        if (req.body.email && req.body.password) {
            const user = await User.findOne({email: req.body.email});
            if (user) {
                const isCorrect: boolean = await bcrypt.compare(req.body.password, user.password);

                if (isCorrect) {
                    req['auth'] = {
                        id: user._id,
                        username: user.username,
                        email: user.email
                    };
                    next();
                } else {
                    return res.status(401).send('Wrong password');
                }

            } else {
                return res.status(401).send('No such user');
            }
        }
    }

    /**
     * Take each handler, and attach to one of the Express.Router's
     * endpoints.
     */
    init() {

        this.router.get('/auth/check', authenticate, this.checkAuth);

        this.router.get('/me', authenticate, this.getCurrentUser);

        this.router.post('/auth/register',
            this.register,
            generateToken,
            sendAuth
        );

        this.router.post('/auth/login',
            this.login,
            generateToken,
            sendAuth
        );

        this.router.post('/auth/facebook',
            passport.authenticate('facebook-token', {session: false}),
            this.authFacebook,
            generateToken,
            sendAuth
        );
        return this;
    }

}

// Create the HeroRouter, and export its configured Express.Router
export default new UsersRouter().init().router;
