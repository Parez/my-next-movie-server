/**
 * Created by baunov on 07/11/2017.
 */
import * as jwt from 'jsonwebtoken';

export const jwtSecret = 'fsoljhdfgaoidfuhoqgo83ygr8q347g98qf8qywgef89yq34g98yfgq3gy8qf97wqef97yg';

const createToken = (auth) => {
    return jwt.sign(
        {id: auth.id},
        jwtSecret,
        {expiresIn: 60 * 120});
};

export const generateToken = (req, res, next) => {
    req.token = createToken(req.auth);
    next();
};

export const sendAuth = (req, res) => {
    console.log(req.token);
    res.setHeader('x-auth-token', req.token);
    res.status(200).send(req.auth);
};
