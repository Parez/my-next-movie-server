import {Router, Request, Response, NextFunction} from 'express';
import { Observable } from 'rxjs';
import {MovieInfo, IMovieInfo, IMovieInfoModel} from '../models/movie-info.model';
import {authenticate} from '../auth/token-auth';
import {UserCollection} from '../models/user-collection.model';

export class CollectionsRouter {
  router: Router;

  /**
   * Initialize the HeroRouter
   */
  constructor() {
    this.router = Router();
    this.init();
  }

  public addToCollection(req: Request, res: Response, next: NextFunction) {
      const userId: string = req['auth'].id;
      const movieId: number = req.params.movieId;

      UserCollection.addToCollection(userId, movieId).subscribe( (data) => {
          res.send(data);
      });
  }

    public removeFromCollection(req: Request, res: Response, next: NextFunction) {
        const userId: string = req['auth'].id;
        const movieId: number = req.params.movieId;

        UserCollection.removeFromCollection(userId, movieId).subscribe( (data) => {
            res.send(data);
        });
    }

    public getCollection(req: Request, res: Response, next: NextFunction) {
        const userId: string = req['auth'].id;
        const status = res.statusCode;

        UserCollection.getCollection(userId).subscribe( (movies) => {
            res.json({
                status,
                data: movies
            });
        }, (error) => {
            res.json({
                status,
                error
            });
        });
    }

    public isInCollection(req: Request, res: Response, next: NextFunction) {
        const userId: string = req['auth'].id;
        const movieId: number = req.params.movieId;
        const status = res.statusCode;

        UserCollection.isInCollection(userId, movieId).subscribe( (isIn) => {
            res.json({
                status,
                data: isIn
            });
        }, (error) => {
            res.json({
                status,
                error
            });
        });
    }

    public getCollectionMovies(req: Request, res: Response, next: NextFunction) {
        const userId: string = req['auth'].id;
        const status = res.statusCode;

        UserCollection.getCollection(userId).switchMap((ids) => {
            if (ids.length === 0) {
                return Observable.of([]);
            }
            return Observable.forkJoin(
                ids.map(id => MovieInfo.getMovieInfoByImdbId(id))
            );
        }).take(1).subscribe( (movies) => {
            return res.json({
                status,
                data: movies
            });
        }, (error) => {
            return res.json({
                status,
                error
            });
        });
    }

  /**
   * Take each handler, and attach to one of the Express.Router's
   * endpoints.
   */
  init() {
    this.router.post('/add/:movieId', authenticate, this.addToCollection);
    this.router.post('/remove/:movieId', authenticate, this.removeFromCollection);
    this.router.get('/', authenticate, this.getCollectionMovies);
    this.router.get('/in/:movieId', authenticate, this.isInCollection);
    return this;
  }

}

// Create the HeroRouter, and export its configured Express.Router
export default new CollectionsRouter().init().router;
