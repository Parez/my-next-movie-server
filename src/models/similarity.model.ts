/**
 * Created by baunov on 08/11/2017.
 */
import * as mongoose from 'mongoose';
import * as request from 'request';

export interface ISimilarity {
    movie_id1: number;
    movie_id2: number;
    score: number;
}

interface ISimilarityModel extends ISimilarity, mongoose.Document {}

export const SimilaritySchema: mongoose.Schema = new mongoose.Schema({
  movie_id1: {
        type: Number
    },
  movie_id2: {
        type: Number
    },
  score: {
        type: Number
    }
});

const SimilarityModel = mongoose.model<ISimilarityModel>('similarities', SimilaritySchema, 'similarities');

export class Similarity extends SimilarityModel {

    constructor(item: ISimilarityModel) {
        super();
        if (!item) {
            return;
        }
        this.movie_id1 = item.movie_id1;
        this.movie_id2 = item.movie_id2;
        this.score = item.score;
    }

    public static async getSimilarities(movieId: number): Promise<ISimilarity[]> {
      const similarities: ISimilarity[] = await Similarity.find({movie_id1: movieId});

      if (similarities.length === 0) {
        await Similarity.create({movie_id1: movieId, movie_id2: movieId, score: 0});
        const sparkUrl = `https://spark.my-next-movie.com/similarities?movieId=${movieId}&apiKey=${process.env.SPARK_API_KEY}`;
        request.get(sparkUrl, {rejectUnauthorized: false}, (res) => {
          // console.log(res);
        });
        return [];
      } else {
        await Similarity.remove({movie_id1: movieId, movie_id2: movieId});
        return similarities.filter(sim => sim.movie_id2 !== sim.movie_id1);
      }
    }
}

