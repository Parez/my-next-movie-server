FROM node:8

# install required OS tools missing in node image
RUN apt-get update
RUN apt-get -qq update
RUN apt-get install net-tools

# run package install
COPY . /data

RUN cd /data; npm install -q --no-progress;
RUN cd /data; npm run build;

# expose the http port
EXPOSE 3000
ENV PORT 3000

# set the working folder to our volume
WORKDIR /data

# run the server
CMD [ "npm", "run", "start:prod" ]