/**
 * Created by baunov on 08/11/2017.
 */
import * as mongoose from 'mongoose';

export interface IMovie {
    movie_id: number;
    title: string;
}

interface IMovieModel extends IMovie, mongoose.Document {}

export const MovieSchema: mongoose.Schema = new mongoose.Schema({
    title: {
        type: String
    },
    movie_id: {
        type: Number
    }
});

const MovieModel = mongoose.model<IMovieModel>('movie_ids', MovieSchema);

export class Movie extends MovieModel {

    constructor(movie: IMovieModel) {
        super();
        if (!movie) return;
        this.movie_id = movie.movie_id;
        this.title = movie.title;
    }

    public static getIMDBIdString(imdbId: number): string {
        // 54594
        // tt0054594
        const properLength = 7;
        const idLength: number = String(imdbId).length;
        let outStr = 'tt';
        for (let i = 0; i < (properLength - idLength); i++) {
            outStr += '0';
        }
        outStr += String(imdbId);
        return outStr;
    }
}