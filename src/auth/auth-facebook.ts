/**
 * Created by baunov on 07/11/2017.
 */
import * as passport from 'passport';
import authConfig from '../config/social-auth';
import { User } from '../models';
const FacebookTokenStrategy = require('passport-facebook-token');

export const initFacebookAuth = () => {
    passport.use(new FacebookTokenStrategy(authConfig.facebookAuth,
    function (accessToken, refreshToken, profile, done) {
        saveFacebookUser(accessToken, refreshToken, profile, function(err, user) {
            return done(err, user);
        });
    }));
};

function saveFacebookUser(accessToken, refreshToken, profile, done) {
    console.log('saveFacebookUser');
    User.findOne({ 'facebook.id': profile.id }).then( (user) => {
        if (user) {
            return done(null, user);
        } else {
            console.log(profile);
            User.create({
                email: profile.email || '',
                username: profile.displayName,
                facebook: {
                    displayName: profile.displayName,
                    token: accessToken,
                    gender: profile.gender,
                    id: profile.id
                }

            }).then((newUser) => {
                return done(null, newUser);
            }).catch(err => {
                return done(err, null);
            });
        }
    }).catch( (err) => {
        return done(err, null);
    });
}
