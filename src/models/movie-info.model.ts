/**
 * Created by baunov on 08/11/2017.
 */
import * as mongoose from 'mongoose';
import * as moment from 'moment';
import { Observable } from 'rxjs/Observable';
import * as rpromise from 'request-promise-native';
import { Movie } from './movie.model';
import { OMDB_API_URL, OMDB_API_KEY, OMDB_POSTERS_URL } from '../api-omdb';
import { YOUTUBE_API_KEY } from '../api-youtube';
const youtube = require('googleapis').youtube;

export interface IMovieInfo {
    Title: string;
    Year: number;
    imdbID: number;
    Poster: string;
    imdbRating: number;
    imdbVotes: number;
    Plot: string;
    Website: string;
    Released: Date;
    YoutubeTrailer: string;
    Languages: string[];
    Productions: string[];
    Writers: string[];
    Countries: string[];
    Genres: string[];
    Actors: string[];
    Directors: string[];
    BigPoster: string;
}

export interface IMovieInfoModel extends IMovieInfo, mongoose.Document {}

export const MovieInfoSchema: mongoose.Schema = new mongoose.Schema({
    Title: {
        type: String
    },
    Year: {
        type: Number
    },
    imdbID: {
        type: Number
    },
    Poster: {
        type: String
    },
    imdbRating: {
        type: Number
    },
    imdbVotes: {
        type: Number
    },
    Plot: {
        type: String
    },
    Website: {
        type: String
    },
    Released: {
        type: Date
    },
    YoutubeTrailer: {
        type: String
    },
    Languages: {
        type: [String]
    },
    Genres: {
        type: [String]
    },
    Countries: {
        type: [String]
    },
    Actors: {
        type: [String]
    },
    Directors: {
        type: [String]
    },
    Productions: {
        type: [String]
    },
    Writers: {
        type: [String]
    },
});

export const MovieInfoModel = mongoose.model<IMovieInfoModel>('movies_info', MovieInfoSchema);

export class MovieInfo extends MovieInfoModel {
    Title: string;
    Year: number;
    imdbID: number;
    Poster: string;
    imdbRating: number;
    imdbVotes: number;
    Plot: string;
    Website: string;
    Released: Date;
    YoutubeTrailer: string;
    Languages: string[];
    Countries: string[];
    Genres: string[];
    Directors: string[];
    Actors: string[];
    Productions: string[];
    Writers: string[];
    BigPoster: string;

    constructor(movieInfo: IMovieInfoModel) {
        super();
        if (!movieInfo) {
            return;
        }
        this.Title = movieInfo.Title;
        this.Year = movieInfo.Year;
        this.imdbID = movieInfo.imdbID;
        this.Poster = movieInfo.Poster;
        this.imdbRating = movieInfo.imdbRating;
        this.imdbVotes = movieInfo.imdbVotes;
        this.Genres = movieInfo.Genres;
        this.Plot = movieInfo.Plot;
        this.Countries = movieInfo.Countries;
        this.Actors = movieInfo.Actors;
        this.Directors = movieInfo.Directors;
        this.Website = movieInfo.Website;
        this.Released = movieInfo.Released;
        this.YoutubeTrailer = movieInfo.YoutubeTrailer;
        this.Languages = movieInfo.Languages;
        this.Productions = movieInfo.Productions;
        this.Writers = movieInfo.Writers;
        this.BigPoster = movieInfo.BigPoster;
    }

    private static filterNAField(field: string): string {
        return field === 'N/A' ? '' : field;
    }

    private static cleanData(data: any): any {
        return {
            Title: MovieInfo.filterNAField(data.Title) || '',
            Year: MovieInfo.filterNAField(data.Year) || '',
            imdbID: MovieInfo.filterNAField(data.imdbID) || '',
            Poster: MovieInfo.filterNAField(data.Poster) || '',
            imdbRating: MovieInfo.filterNAField(data.imdbRating) || '',
            imdbVotes: MovieInfo.filterNAField(data.imdbVotes) || '',
            Plot: MovieInfo.filterNAField(data.Plot) || '',
            Website: MovieInfo.filterNAField(data.Website) || '',
            Released: this.getValidDate(data),
            YoutubeTrailer: data.YoutubeTrailer || '',
            Language: MovieInfo.filterNAField(data.Language) || '',
            Genre: MovieInfo.filterNAField(data.Genre) || '',
            Country: MovieInfo.filterNAField(data.Country) || '',
            Actors: MovieInfo.filterNAField(data.Actors) || '',
            Director: MovieInfo.filterNAField(data.Director) || '',
            Production: MovieInfo.filterNAField(data.Production) || '',
            Writer: MovieInfo.filterNAField(data.Writer) || ''
        };
    }

    public static removeSpecialChars(str) {
        return str.replace(/(?!\w|\s)./g, '')
            .replace(/\s+/g, ' ')
            .replace(/^(\s*)([\W\w]*)(\b\s*$)/g, '$2');
    }

    public static fromOMDBData(data): IMovieInfo {
        const clean = MovieInfo.cleanData(data);
        const info = {
            Title: clean.Title,
            Year: clean.Year.slice(0, 4),
            imdbID: +clean.imdbID.split('tt')[1],
            Poster: clean.Poster,
            imdbRating: Number.parseFloat(data.imdbRating) || 0,
            imdbVotes: Number.parseInt(data.imdbVotes) || 0,
            Plot: clean.Plot,
            Website: clean.Website,
            Released: this.getValidDate(data),
            YoutubeTrailer: clean.YoutubeTrailer,
            BigPoster: clean.BigPoster,
            Languages: clean.Language.length > 0 ? clean.Language.split(', ') : [],
            Genres: clean.Genre.length > 0 ? clean.Genre.split(', ') : [],
            Countries: clean.Country.length > 0 ? clean.Country.split(', ') : [],
            Actors: clean.Actors.length > 0 ? clean.Actors.split(', ') : [],
            Directors: clean.Director.length > 0 ? clean.Director.split(', ') : [],
            Productions: clean.Production.length > 0 ? clean.Production.split(' / ') : [],
            Writers: clean.Writer.length > 0 ? clean.Writer.split(', ') : []
        };
        return info;
    }

    private static getValidDate(data): Date {
        if (moment.isDate(data.Relased))
            return moment(data.Released).toDate();
        else {
            return moment(`1 Jan ${data.Year.slice(0, 4)}`).toDate();
        }
    }

    public static async getMovieTrailer(name: string, year: number): Promise<any> {
        return new Promise((resolve, reject) => {
          youtube('v3').search.list({
            auth: YOUTUBE_API_KEY,
            part: 'snippet',
            q: name + ' trailer ' + String(year),
            maxResults: 1
          }, (err, res) => {
            console.log("Response", res['items'][0].id.videoId);
            if (err) {
              reject(err);
            } else {
              resolve(res['items'][0].id.videoId);
            }
          });
        });
    }

    private static getRegexSearchAr(str: string, searchField: string): any[] {
        return str.split(' ').map((word) => {
            return new RegExp(`^${word}`, 'i');
        });
    }

    public static async searchMovies(field: string,
                               search: string,
                               skip: number,
                               limit: number,
                               sortOn: string,
                               sortOrder: number,
                               includeFields: any = {}): Promise<any[]> {

        // const searchWords = this.getRegexSearchAr(search, field);
        const sortObj = sortOn === '' ? {score: {$meta: 'textScore'}} : {[sortOn]: sortOrder};

        let searchQuery;
        if (field === 'SearchField') {
            searchQuery = MovieInfo.aggregate([
                { $match: {$text: {$search: search}} },
                { $addFields: {score: {$meta: 'textScore'}} },
                { $match: {score: {$gte: search.split(' ').length}} },
                { $sort: sortObj}
            ]).skip(skip).limit(limit);

            /*find({$text: {$search: search}}, {score: {$meta: 'textScore'}})
                .sort(sortObj)
                .skip(skip)
                .limit(limit);*/
        } else if (field === '') {
            searchQuery = MovieInfo.find({}, includeFields)
                .sort({[sortOn]: sortOrder})
                .skip(skip)
                .limit(limit);
        } else {
            searchQuery = MovieInfo.find({[field]: { $elemMatch : { $regex : search, $options : 'i' }}}, includeFields)
                .sort({[sortOn]: sortOrder})
                .skip(skip)
                .limit(limit);
        }

        const foundMovies = await searchQuery;

        return foundMovies;
    }

    public static findMovies(queryObj: any = {}): Observable<MovieInfo[]> {
        return Observable.defer(() => MovieInfo.find(queryObj));
    }

    private static async getOMDBMovieData(id: number): Promise<string> {
      const imdbIdString = Movie.getIMDBIdString(id);
      const dataQuery = {
        uri: OMDB_API_URL,
        qs: {
          i: imdbIdString,
          apikey: OMDB_API_KEY
        }
      };
      try {
        const data = await rpromise.get(dataQuery);
        return data;
      } catch (err) {
        console.log("Data", err);
        return '';
      }
    }

    public static async getOMDBPoster(id: number): Promise<any> {
      const imdbIdString = Movie.getIMDBIdString(id);
      const posterQuery = {
        uri: OMDB_POSTERS_URL,
        qs: {
          i: imdbIdString,
          apikey: OMDB_API_KEY
        }
      };

      return rpromise.get(posterQuery).then(poster => {
        console.log("Loading Poster");
        return poster;
      }).catch(err => {
        console.log("No poster");
        return '';
      });
    }

    public static async getMovieInfoByImdbId(id: number): Promise<IMovieInfo> {
      const existingMovie = await MovieInfo.findOne({imdbID: id});

      if (!existingMovie) {
        console.log("Not Exists");
        const movieData = await MovieInfo.getOMDBMovieData(id);
        console.log("movie", movieData);
        const parsedMovieData = JSON.parse(movieData);
        console.log("parsedMovie", parsedMovieData);
        let posterUrl = '';
        try {
          posterUrl = await MovieInfo.getOMDBPoster(id);
        } catch (err) {
          console.log(err);
        }

        console.log("Poster", posterUrl);

        let trailer = '';
        try {
          trailer = await MovieInfo.getMovieTrailer(parsedMovieData.Title, parsedMovieData.Year);
        } catch (err) {
          console.log("Trailer", err);
        }
        console.log("Trailer", trailer);
        const newMovie = MovieInfo.fromOMDBData(Object.assign(
          {},
          parsedMovieData,
          {YoutubeTrailer: trailer, BigPoster: posterUrl}
        ));
        try {
          await MovieInfo.create(newMovie);
        } catch (err) {
          console.log("Failed to create");
        }

        return newMovie;
      } else {
        return existingMovie;
      }
    }
}

