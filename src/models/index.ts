/**
 * Created by baunov on 08/11/2017.
 */
export * from './user.model';
export * from './movie.model';
export * from './movie-info.model';
export * from './similarity.model';
export * from './user.model';
export * from './user-movie-rating.model';
export * from './user-collection.model';
export * from './recommendation.model';
