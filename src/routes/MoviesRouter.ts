import {Router, Request, Response, NextFunction} from 'express';
import { Movie } from '../models';
import {OMDB_API_KEY, OMDB_API_URL, OMDB_POSTERS_URL} from '../api-omdb';
import * as request from 'request';
import { Observable } from 'rxjs/Observable';
import {MovieInfo, IMovieInfo, IMovieInfoModel} from '../models/movie-info.model';
import {IUserMovieRating, UserMovieRating} from '../models/user-movie-rating.model';
import {authenticate} from '../auth/token-auth';
import {Recommendation} from '../models/recommendation.model';
import {Similarity} from '../models/similarity.model';

export class MoviesRouter {
    router: Router;

    /**
     * Initialize the HeroRouter
     */
    constructor() {
        this.router = Router();
        this.init();
    }

    public loadAllMovies(req: Request, res: Response, next: NextFunction) {
        let allMovies = 100;
        let curMovie = 0;
        Observable.defer(() => Movie.find())
            .do((movies) => allMovies = movies.length)
            .map((movies) => movies.map((movie) => movie.movie_id))
            .flatMap((ids: number[]) => {
                return Observable.from(ids);
            })
            .zip(Observable.interval(100), function(a, b) { return a; })
            .flatMap( (id: number) => {
                console.log(id);
                return MovieInfo.getMovieInfoByImdbId(id);
            })
            .do(() => curMovie += 1)
            .subscribe((movie) => console.log(movie), (err) => {
                // console.log(err);
                res.status(500).send(err);
            }, () => {
                if (curMovie >= allMovies) {
                    res.status(200).send('It worked!');
                }
            });
    }

    // Returns IMovieBase[]
    public searchMovies(req: Request, res: Response, next: NextFunction) {
        let search: string;
        if (req.params.search) {
            search = req.params.search.trim() || '';
        } else {
            search = '';
        }

        const numSkip: number = req.query.skip | 0;
        const numLimit: number = req.query.limit | 10;
        const sortOn: string = req.query.sortOn || '';
        let sortOrder: number = req.query.sortOrder | -1;
        const field: string = req.query.field || '';

        const status = res.statusCode;

        if (sortOrder !== 1 && sortOrder !== -1) {
            sortOrder = -1;
        }

        return MovieInfo.searchMovies(field, search, numSkip, numLimit, sortOn, sortOrder).then( (movies) => {
            res.json({
                status,
                data: movies
            });
        }, (error) => {
            res.json({
                status,
                error
            });
        });
    }

    public getRecentMovies(req: Request, res: Response, next: NextFunction) {
        const numSkip: number = req.query.skip | 0;
        const numLimit: number = req.query.limit | 10;
        Observable.defer(() => {
            return MovieInfo.find({})
                .sort({'Relased': -1})
                .skip(numSkip)
                .limit(numLimit);
        }).subscribe( (movies) => {
            res.json({
                status,
                data: movies
            });
        }, (error) => {
            res.json({
                status,
                error
            });
        });
    }

    public getMoviesByNameSearch(req: Request, res: Response, next: NextFunction) {

        const name = req.params.name || '';
        const status = res.statusCode;

        MovieInfo.searchMovies('Title', name, 0, 20, 'Released', -1,
            {imdbID: true, Title: true, _id: false})
            .then( (movies) => {
                res.json({
                    status,
                    data: movies
                });
            }, (error) => {
                res.json({
                    status,
                    error
                });
            });
    }

    public getMovieInfoByImdbId(req: Request, res: Response, next: NextFunction) {

        const status = res.statusCode;
        const id = req.params.id;

        return MovieInfo.getMovieInfoByImdbId(id).then((info) => {
          return res.json({
            status,
            data: info
          });
        });
    }

    public getRecommendations(req: Request, res: Response, next: NextFunction) {
        const status = res.statusCode;
        const userId: string = req['auth'].id;
        return Recommendation.getRecommendations(userId).then(recs => {
          return res.json({
            status,
            data: recs
          });
        }).catch(error => {
          return res.json({
            status,
            error
          });
        });
    }

    public async likeMovie(req: Request, res: Response, next: NextFunction) {
      const status = res.statusCode;
      const movieId = req.params.id;
      const userId: string = req['auth'].id;

      const curRating: IUserMovieRating = await UserMovieRating.findOne({movieId, userId});
      if (curRating && curRating.rating > 0) {
        const count: number = await UserMovieRating.countReactions(userId);
        return res.json({
          status,
          data: count
        });
      }

      return UserMovieRating.rateMovie(userId, movieId, 1).then( (data) => {
        return res.json({
          status,
          data
        });
      }).catch(error => {
        return res.json({
          status,
          error
        });
      });
    }

    public async dislikeMovie(req: Request, res: Response, next: NextFunction) {
        const status = res.statusCode;
        const movieId = req.params.id;
        const userId: string = req['auth'].id;

        const curRating: IUserMovieRating = await UserMovieRating.findOne({movieId, userId});
        if (curRating && curRating.rating < 0) {
          const count: number = await UserMovieRating.countReactions(userId);
          return res.json({
            status,
            data: count
          });
        }

        return UserMovieRating.rateMovie(userId, movieId, -1).then( (data) => {
          return res.json({
            status,
            data
          });
        }).catch(error => {
          return res.json({
            status,
            error
          });
        });
    }

    public getLikes(req: Request, res: Response, next: NextFunction) {
      const status = res.statusCode;
      const userId: string = req['auth'].id;
      return UserMovieRating.getLikes(userId).then(movies => {
        // console.log(movies);
        return res.json({
          status,
          data: movies
        });
      }).catch(error => {
        return res.json({
          status,
          error
        });
      });
    }

    public getDislikes(req: Request, res: Response, next: NextFunction) {
      const status = res.statusCode;
      const userId: string = req['auth'].id;
      return UserMovieRating.getDislikes(userId).then(movies => {
        return res.json({
          status,
          data: movies
        });
      }).catch(error => {
        return res.json({
          status,
          error
        });
      });
    }

    public getMovieReaction(req: Request, res: Response, next: NextFunction) {
        console.log("Reactions", req.params.id, req['auth']);
        const movieId = req.params.movieId;
        const userId: string = req['auth'].id;
        const status = res.statusCode;

        return UserMovieRating.findOne({userId, movieId}).then( (data: IUserMovieRating) => {
            return res.json({
              status,
              data: data.rating
            });
        }).catch(error => {
          return res.json({
            status,
            error
          });
        });
    }

  public numReactions(req: Request, res: Response, next: NextFunction) {
    const userId: string = req['auth'].id;
    const status = res.statusCode;

    return UserMovieRating.countReactions(userId).then( (data: number) => {
      return res.json({
        status,
        data
      });
    });
  }

  public async getMoviePoster(req: Request, res: Response, next: NextFunction) {
    const movieId = req.params.movieId;

    const imdbIdString = Movie.getIMDBIdString(movieId);
    const posterQuery = {
      uri: OMDB_POSTERS_URL,
      qs: {
        i: imdbIdString,
        apikey: OMDB_API_KEY
      }
    };

    return request(posterQuery).pipe(res);
  }

  public async getMovieTrailer(req: Request, res: Response, next: NextFunction) {
    const movieId = req.params.movieId;
    const status = res.statusCode;

    const movie = await MovieInfo.findOne({imdbID: movieId});

    return MovieInfo.getMovieTrailer(movie.Title, movie.Year).then(trailer => {
      console.log("Trailer ", trailer);
      return res.json({
        status,
        data: trailer
      });
    }).catch(error => {
      return res.json({
        status,
        error
      });
    });
  }

  public async getSimilarities(req: Request, res: Response, next: NextFunction) {
    const movieId = req.params.movieId;
    const status = res.statusCode;
    const similarities = await Similarity.getSimilarities(movieId);
    try {
      const movies =  await MovieInfo.find({imdbID: {$in: similarities.map(sim => sim.movie_id2)}});
      const withScores = movies.map(movie => {
        // console.log(movie);
        return Object.assign({}, movie.toObject(), {score: similarities.filter(sim => sim.movie_id2 === movie.imdbID)[0].score});
      }).sort((s1, s2) => {
        if (s1.score > s2.score) {
          return -1;
        } else {
          return 1;
        }
      });

      // console.log(withScores);
      return res.json({
        status,
        data: withScores
      })
    } catch (error) {
      return res.json({
        status,
        error
      });
    }
  }


    /**
     * Take each handler, and attach to one of the Express.Router's
     * endpoints.
     */
    init() {
        this.router.get('/search/:search', this.searchMovies);
        this.router.get('/poster/:movieId', this.getMoviePoster);
        this.router.get('/trailer/:movieId', this.getMovieTrailer);
        this.router.get('/search/', this.searchMovies);
        this.router.get('/id/:id', this.getMovieInfoByImdbId);
        this.router.get('/name/:name', this.getMoviesByNameSearch);
        this.router.get('/name/', this.getMoviesByNameSearch);
        this.router.post('/like/:id', authenticate, this.likeMovie);
        this.router.post('/dislike/:id', authenticate, this.dislikeMovie);
        this.router.get('/reaction/:movieId', authenticate, this.getMovieReaction);
        this.router.get('/recommendations', authenticate, this.getRecommendations);
        this.router.get('/likes', authenticate, this.getLikes);
        this.router.get('/dislikes', authenticate, this.getDislikes);
        this.router.get('/numReactions', authenticate, this.numReactions);
        this.router.get('/similarities/:movieId', this.getSimilarities);
        return this;
    }

}

// Create the HeroRouter, and export its configured Express.Router
export default new MoviesRouter().init().router;
