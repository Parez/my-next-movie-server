import * as path from 'path';
import * as express from 'express';
import * as logger from 'morgan';
import * as bodyParser from 'body-parser';
import * as mongoose from 'mongoose';
import MoviesRouter from './routes/MoviesRouter';
import * as cors from 'cors';
import UserRouter from './routes/UsersRouter';
import CollectionsRouter from './routes/CollectionsRouter';
import * as passport from 'passport';
import {initFacebookAuth} from './auth/auth-facebook';
import * as dotenv from 'dotenv';
// Creates and configures an ExpressJS web server.
dotenv.config();

const MONGO_PASSWORD = process.env.MONGO_PASSWORD;
const MONGO_USERNAME = process.env.MONGO_USERNAME;
const MONGO_DATABASE = process.env.MONGO_DATABASE;
const MONGO_CLUSTERS = process.env.MONGO_CLUSTERS;

let MONGO_URI = `localhost:27017/movie_db`;

if (MONGO_DATABASE && MONGO_PASSWORD && MONGO_USERNAME && MONGO_CLUSTERS) {
  MONGO_URI = `mongodb://${MONGO_USERNAME}:${MONGO_PASSWORD}@${MONGO_CLUSTERS}${MONGO_DATABASE}?ssl=true&replicaSet=Cluster0-shard-0&authSource=admin`;
}


const corsOption = {
  origin: true,
  methods: 'GET,HEAD,PUT,PATCH,POST,DELETE',
  credentials: true,
  exposedHeaders: ['x-auth-token']
};


class App {

  // ref to Express instance
  public express: express.Application;

  // Run configuration methods on the Express instance.
  constructor() {
    this.express = express();
    this.config();
    this.middleware();
    this.routes();
  }

  private config() {
    mongoose.connect(MONGO_URI || process.env.MONGODB_URI).catch(err => {
        console.log(err);
    });
  }

  // Configure Express middleware.
  private middleware(): void {
    this.express.use(function(req, res, next) {
      res.header('Access-Control-Allow-Origin', '*');
      res.header('Access-Control-Allow-Headers', 'Origin, X-Requested-With, Content-Type, Accept');
      next();
    });
    this.express.use(logger('dev'));
    this.express.use(bodyParser.json());
    this.express.use(bodyParser.urlencoded({ extended: false }));
    this.express.use(cors(corsOption));
    this.express.use(passport.initialize());
    this.express.use(passport.session());
    initFacebookAuth();
  }

  // Configure API endpoints.
  private routes(): void {
    const router = express.Router();
    // placeholder route handler
    router.get('/', (req, res, next) => {
      res.json({
        message: 'Hello World!'
      });
    });
    this.express.use('/', router);
    this.express.use('/api/movies', MoviesRouter);
    this.express.use('/api/users', UserRouter);
    this.express.use('/api/collections', CollectionsRouter);
  }

}

export default new App().express;
