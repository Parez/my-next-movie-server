/**
 * Created by baunov on 07/11/2017.
 */
import * as expressJwt from 'express-jwt';
import {jwtSecret} from './jwt';

export const authenticate = expressJwt({
    secret: jwtSecret,
    requestProperty: 'auth',
    getToken: function(req) {
        if (req.headers['x-auth-token']) {
            return req.headers['x-auth-token'];
        }
        return null;
    }
});
